from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in hcms_survey/__init__.py
from hcms_survey import __version__ as version

setup(
	name="hcms_survey",
	version=version,
	description="Masterise Survet for Frappe",
	author="Mas",
	author_email="MAS",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
