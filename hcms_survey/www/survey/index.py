import frappe
import json
import jwt
from hcms_survey.config.constant import *

def get_context(context):

	user1 = frappe.get_doc('User', frappe.session.user)

	token = frappe.request.args["token"];
	payload = jwt.decode(token, JWT_SCRET_KEY, options={"verify_signature": False}) or {}

	if not payload["id"]:
		context.result = 0
		return context

	userSurvey = frappe.get_doc('User Survey', payload["id"]).as_dict()

	context.user = frappe.get_doc('User', userSurvey.user).as_dict()

	survey = frappe.get_doc('Survey', userSurvey["survey"]).as_dict()

	survey.elements = []

	for collectionElement in survey.collection_survey_element:
		element = frappe.get_doc('Survey Element', collectionElement.survey_element)
		if element.question:
			element.question = get_question(element.question)
		survey.elements.append(element)
		
	context.main_title = survey.label
	context.description = survey.description
	context.survey = survey
	context.user1 = user1

	return context

def get_question(id):
	if not id:
		return []

	question = frappe.get_doc('Question', id).as_dict()

	if question.collection_metadata_answer:
		question.metadata_answers = []
		for metadata_answer in question.collection_metadata_answer:
			answer = frappe.get_doc('Metadata Answer', metadata_answer.metadata_answer).as_dict();
			if (answer.collection_question):
				answer.questions = [];
				for metadata_question in answer.collection_question:
					answer.questions.append(get_question(metadata_question.question))
			question.metadata_answers.append(answer)

	if (question.collection_question):
		question.questions = []
		for metadata_question in question.collection_question:
			question.questions.append(get_question(metadata_question.question))

	return question
