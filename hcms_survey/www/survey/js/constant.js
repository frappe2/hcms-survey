const ELEMENT_TYPE_H1 = "Heading 1";
const ELEMENT_TYPE_H2 = "Heading 2";
const ELEMENT_TYPE_H3 = "Heading 3";
const ELEMENT_TYPE_QUESTION = "Question";

const QUESTION_TYPE_TEXT = "Text";
const QUESTION_TYPE_TEXTAREA = "Textarea";
const QUESTION_TYPE_RADIO = "Radio";
const QUESTION_TYPE_NUMBER = "Number";

const COLUMN_BY_PERCENT = {
	"25%": 3,
	"50%": 6,
	"75%": 9,
	"100%": 12,
}
