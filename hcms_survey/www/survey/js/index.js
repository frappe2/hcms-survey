const ELEMENTS = {
	"survey_element": document.getElementById("survey_body")
}

for(const element of survey.elements) {
	let html = "";
	switch(element.type) {
		case ELEMENT_TYPE_H2: {
			html = `
				<h2 class="h2-title">${element.index || ""} ${element.label}</h2>
				<span class="h2-description">${element.description}</span>
			`
			break;
		}
		case ELEMENT_TYPE_H3: {
			html = `
				<h3 class="h3-title">${element.index || ""} ${element.label}</h3>
				<span class="h3-description">${element.description}</span>
			`
			break;
		}
		case ELEMENT_TYPE_QUESTION: {
			let htmlLabel = "";
			if (element.is_render_label) {
				htmlLabel = `<label for="" class="form-label">
								<p class="question-title">${element.index || ""} ${element.question.label}</p>
								<p class="question-description">${element.question.description}</p>
							</label>`
			}
			if (element.question.type == "Group") {
				render_element(render_element_html(element, render_question_html(element.question, htmlLabel)))
				for(let i = 0; i < element.question.questions.length; i++) {
					const elementQuestion = element.question.questions[i];
					if (elementQuestion.type == "Matrix") {
						let htmlHead = "<th></th>";
						let htmlBody = "";
						for(const answer of elementQuestion.metadata_answers) {
							htmlHead += `
								<th>${answer.label}</th>
							`
						}
						while (element.question.questions[i].type == "Matrix") {
							console.log("1", element.question.questions[i]);
							let htmlRow = `<td class="text-start ps-2 py-2">${element.question.questions[i].label}<br><span class="td-description">${element.question.questions[i].description}</span></td>`;
							for(const answer of element.question.questions[i].metadata_answers) {
								htmlRow += `
								<td>
									<input type="radio" name="${element.question.questions[i].name}">
								</td>
								`
							}
							htmlBody += `
							<tr>
								${htmlRow}
							</tr>
							`
							i++;
							if (i == element.question.questions.length) break;
						}
						i--;
						html = `
						<table class="w-100">
							<thead>
								<tr>
								${htmlHead}

								</tr>
							</thead>
							<tbody>
								${htmlBody}
							</tbody>
						</table>
						`
						render_element(`
							<div class="col-md-12 element-item">
								${html}
							</div>
						`);
					} else {
						render_element(render_element_html(elementQuestion, render_question_html(elementQuestion, "")));
					}
				}
				continue;
			} else {
				html = render_question_html(element.question, htmlLabel)
			}
			break;
		}
	}
	html = render_element_html(element, html);
	render_element(html);
}

function render_element(html) {
	ELEMENTS.survey_element.insertAdjacentHTML('beforeend', html);
}

function render_element_html(element, html) {
	return `
	<div style='color: ${element.color}' class="col-md-${COLUMN_BY_PERCENT[element.width]} element-item" data-type="${element.type}">
		${html}
	</div>
	`
}

function render_question_html(question, htmlLabel) {
	if (question.type == "Group") {
		return `<div>
					${htmlLabel}
				</div>`
	}

	let html = "";

	switch (question.answer_type) {
		case QUESTION_TYPE_TEXT: {
			html = `
			<div>
				${htmlLabel}
				<input type="text" class="form-control" id="" placeholder="${question.placeholder || ''}">
			</div>
			`
			break;
		}
		case QUESTION_TYPE_NUMBER: {
			html = `
			<div>
				${htmlLabel}
				<input type="number" class="form-control" id="" placeholder="${question.placeholder || ''}">
			</div>
			`
			break;
		}
		case QUESTION_TYPE_TEXTAREA: {
			html = `
			<div>
				${htmlLabel}
				<textarea class="form-control" rows=4 placeholder="${question.placeholder || ''}"></textarea>
			</div>
			`
			break;
		}
		case QUESTION_TYPE_RADIO: {
			for (const item of question.metadata_answers) {
				let htmlExtraQuestion = "";
				if (item.questions) {
					for (const extraQuestion of item.questions) {
						htmlExtraQuestion += `
							<div class="col-md-6">
							
							</div>
						`
					}
				}
				html += `
				<div class="radio-item-wrapper">
					<input type="radio" name="q_${question.name}" id="a_${question.name}_${item.name}"> <label for="a_${question.name}_${item.name}">${item.label}</label>
					${htmlExtraQuestion}	
				</div>
				`

	
			}
			html = `
			<div class="radio-wrapper flex-column">
				${htmlLabel}
				${html}
			</div>
			`
			break;
		}
	}
	return html;
}