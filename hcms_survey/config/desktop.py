from frappe import _

def get_data():
	return [
		{
			"module_name": "HCMS - Survey",
			"type": "module",
			"label": _("HCMS - Survey")
		}
	]
