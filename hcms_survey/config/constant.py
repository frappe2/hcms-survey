JWT_SCRET_KEY = "hcms"

RESPONSE_RESULT_SUCCESS = 1

TEMPLATE_SURVEY_DETAIL = "hcms_survey/public/templates/pages/detail.html"

class DOCTYPE:
	EMPLOYEE = "Employee"
	USER_SEGMENT = "User Segment"
	USER_SURVEY = "User Survey"
	SURVEY = "Survey"
	USER = "User"
	EVENT = "Events"

class DOCTYPE_SUBMITABLE_STATUS:
	SUBMITTED = 1

class USER_SEGMENT_TYPE:
	CUSTOM = "Custom"
	DEPARTMENT = "Department"
	COMPANY = "Company"

class RESPONSE_HTTP_CODE:
	BAD_REQUEST = 400
