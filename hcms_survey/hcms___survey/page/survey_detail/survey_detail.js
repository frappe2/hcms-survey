const userSurveyId = window.location.pathname.split("/").pop();
let ELEMENTS = null;

frappe.pages['survey-detail'].on_page_load = function(wrapper) {
	const page = new SurveyDetailPage(wrapper);
}

class SurveyDetailPage {
	constructor(wrapper){
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: '',
			single_column: true
		});

		this.wrapper = wrapper;

		this.workspace = "HCMS";

		this.make();
	}

	async make() {
		frappe.call({
			method: API_URL.GET_USER_SURVEY,
			args: {
				id: userSurveyId,
			},
			callback: res => {

				if (res.result != 1) {
					frappe.throw(res.message || "Server is busy !")
					return
				}

				const {survey, answers, user} = res.data;

				this.page.set_title(`${survey.label} - ${user}`);
				// INIT TEMPLATE
				this.page.main.append(frappe.render_template(frappe.templates.detail, {
					main_title: survey.label,
					description: survey.description,
					survey: {}
				}))

				ELEMENTS = {
					"survey_element": document.getElementById("survey_body")
				}
				// RENDER SURVEY ELEMENTS
				for(const element of survey.elements) {
					let html = "";
					const readonly = element.user_in_charge == frappe.session.user ? "" : "read-only";
					switch(element.type) {
						case ELEMENT_TYPE_H2: {
							html = html_h2(element)
							break;
						}
						case ELEMENT_TYPE_H3: {
							html = html_h3(element);
							break;
						}
						case ELEMENT_TYPE_QUESTION: {
							let htmlLabel = "";
							if (element.is_render_label) {
								htmlLabel = html_label_question(element)
							}
							element.question.answered = answers[element.name] || "";
							if (element.question.type == "Group") {
								render_element(html_render_element(element, html_render_question(element.question, htmlLabel)))
								for(let i = 0; i < element.question.questions.length; i++) {
									const elementQuestion = element.question.questions[i];
									if (elementQuestion.type == "Matrix") {
										let htmlHead = "<th></th>";
										let htmlBody = "";
										for(const answer of elementQuestion.metadata_answers) {
											htmlHead += `
												<th>${answer.label}</th>
											`
										}
										while (element.question.questions[i].type == "Matrix") {
											const question = element.question.questions[i];
											let htmlRow = `<td class="text-left pl-2 py-2">${element.question.questions[i].label}<br><span class="td-description">${element.question.questions[i].description}</span></td>`;
											for(const answer of element.question.questions[i].metadata_answers) {
												htmlRow += `
												<td>
													<input type="radio" ${answers[`${element.name}_${element.question.questions[i].name}`] == answer.name ? "checked" : (readonly ? "disabled" : "")} name="${element.name}_${question.name}" value="${answer.name}" id="${element.name}_${question.name}_${answer.name}">
												</td>
												`
											}
											htmlBody += `
											<tr>
												${htmlRow}
											</tr>
											`
											i++;
											if (i == element.question.questions.length) break;
										}
										i--;
										html = html_matrix(htmlHead, htmlBody);
										render_element(`
											<div class="col-md-12 element-item">
												${html}
											</div>
										`);
									} else {
										render_element(html_render_element(elementQuestion, html_render_question(elementQuestion, "")));
									}
								}
								continue;
							} else {
								html = html_render_question(element.question, htmlLabel, readonly)
							}
							break;
						}
					}
					html = html_render_element(element, html);
					render_element(html);
				}
			}
		});
	}
}

$(document).on("submit", "#form_survey", function (e) {
	e.preventDefault();
	frappe.confirm('Are you sure you want to submit ?',
	() => {
		const formData = new FormData(this);
		frappe.call({
			method: API_URL.SUBMIT_SURVEY, //dotted path to server method
			args: {
				id: userSurveyId,
				data: Object.fromEntries(formData)
			},
			callback: res => {
				if (res.result != 1) {
					frappe.throw(res.message || "Server is busy !")
					return
				}

				frappe.show_alert({
					message: res.message || 'Successfully !',
					indicator:'green'
				}, 5);
			}
		});
	}, () => {
		// action to perform if No is selected
	})

})

function render_element(html) {
	ELEMENTS.survey_element.insertAdjacentHTML('beforeend', html);
}
