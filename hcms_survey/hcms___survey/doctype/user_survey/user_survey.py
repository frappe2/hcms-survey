# Copyright (c) 2022, Mas and contributors
# For license information, please see license.txt

import frappe
import json
from frappe.model.document import Document
from hcms_survey.config.constant import *

class UserSurvey(Document):
	def get_user(self):
		return frappe.get_doc(DOCTYPE.USER, self.user)
