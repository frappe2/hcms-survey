// Copyright (c) 2022, Mas and contributors
// For license information, please see license.txt

frappe.ui.form.on('User Survey', {
	refresh: function(frm) {
		frm.add_custom_button('Fill this survey', () => {
			frappe.set_route(`/app/survey-detail/${frm.doc.name}`);
		});
	}
});
