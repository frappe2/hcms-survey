# Copyright (c) 2022, Mas and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document

class SurveyElement(Document):
	pass
	def before_insert(self):
		if self.type == "Question":
			self.label = f'{self.type} - {self.label}'
