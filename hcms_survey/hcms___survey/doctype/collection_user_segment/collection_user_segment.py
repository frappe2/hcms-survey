# Copyright (c) 2022, Mas and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from hcms_survey.config.constant import DOCTYPE


class CollectionUserSegment(Document):

	def get_segment(self):
		return frappe.get_doc(DOCTYPE.USER_SEGMENT, self.segment)
