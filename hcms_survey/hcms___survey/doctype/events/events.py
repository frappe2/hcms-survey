# Copyright (c) 2022, Mas and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from hcms_survey.config.constant import *
from hcms_survey.services.user import UserService


class Events(Document):
	def get_participant(self):
		participant = []
		for segment in self.collection_segment:
			participant = segment.get_segment().get_users()
		return participant

	def get_survey(self):
		return frappe.get_doc(DOCTYPE.SURVEY, self.survey)

	def onload(self):
		participant = self.get_participant()
		print(participant)
		users = []
		for idx, user in enumerate(participant):
			user.idx = idx + 1
			users.append(user)

		self.participant = users


@frappe.whitelist()
def get_participant(id):
	event = frappe.get_doc(DOCTYPE.EVENT, id)
	frappe.response.data = event.get_participant()
	frappe.response.result = RESPONSE_RESULT_SUCCESS


@frappe.whitelist()
def change_state(id, state):
	event = frappe.get_doc(DOCTYPE.EVENT, id)

	survey = event.get_survey()
	survey.docstatus = DOCTYPE_SUBMITABLE_STATUS.SUBMITTED
	survey.save()

	if (not event or not state):
		frappe.response.status_code = RESPONSE_HTTP_CODE.BAD_REQUEST

	participant = event.get_participant()

	for user in participant:
		employee = UserService.get_employee(user.name)
		userSurvey = frappe.new_doc(DOCTYPE.USER_SURVEY)
		userSurvey.user = user.name
		userSurvey.label = event.get_survey().label + ' - ' + user.full_name
		userSurvey.survey = event.survey
		userSurvey.hod = employee.get_hod().work_email
		userSurvey.line_manager = employee.get_line_manager().work_email
		userSurvey.insert()

	frappe.response.data = participant
	frappe.response.result = RESPONSE_RESULT_SUCCESS
