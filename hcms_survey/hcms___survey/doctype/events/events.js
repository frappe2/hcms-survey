// Copyright (c) 2022, Mas and contributors
// For license information, please see license.txt

frappe.ui.form.on('Events', {

	onload(frm) {

		frm.set_query("contact_person", () => {
			return {
				//query: "hcms.human_capital_management_system.doctype.employee.employee_services.search_hr_employee"
				filters: {
					'department': 'HR',
					'status': ['IN', ['Active', 'On Leaves']]
				}
			}
		})
	},

	refresh(frm) {


		frm.add_custom_button(__("Send invite email"), () => {
			frappe.call({
				method: 'hcms_survey.api.v1.events.send_invite_event',
				args: {
					'event_name': frm.doc.name
				}
			})
		})

		if (frm.doc.workflow_state != CONSTANTS.EVENT_STATE.PENDING) return;
		frm.add_custom_button(__("Approve"), function () {

			frappe.confirm('Are you sure you want to approve this event ?',
				() => {
					frm.set_value("workflow_state", "Approved")
					frappe.call({
						method: CONSTANTS.API_URL.EVENT_CHANGE_STATE,
						args: {
							id: frm.doc.name,
							state: CONSTANTS.EVENT_STATE.APPROVED
						},
						error: res => {
							res = res.responseJSON;
							frappe.show_alert({
								message: res.message || 'Failed !',
								indicator: 'red'
							}, 5);
						},
						callback: res => {
							if (res.result != 1) {
								frappe.throw(res.message || "Server is busy !")
								return
							}

							frm.save();

							frappe.show_alert({
								message: res.message || 'Successfully !',
								indicator: 'green'
							}, 5);

						},

					});
				}, () => {
					// action to perform if No is selected
				})
		});
	}
});

// frappe.ui.form.on('Agenda', {


// 	onload(frm, cdt, cdn) {
// 		frm.set_query("contact_person", () => {
// 			return {
// 				query: "hcms.human_capital_management_system.employee.employee_services.get_official_employee"
// 			}
// 		})
// 	},


// 	refresh(frm, cdt, cdn) {
// 		frm.call("get_list_employee_active")
// 	}
// })



