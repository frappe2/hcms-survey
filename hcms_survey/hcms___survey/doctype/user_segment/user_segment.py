# Copyright (c) 2022, Mas and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from hcms_survey.config.constant import *


class UserSegment(Document):
	def get_users(self):
		users = []
		match self.type:
			case USER_SEGMENT_TYPE.CUSTOM:
				for user in self.collection_user:
					users.append(frappe.get_doc(DOCTYPE.USER, user.email))

			case USER_SEGMENT_TYPE.DEPARTMENT:
				for user in self.collection_user:
					users.append(frappe.get_doc(DOCTYPE.USER, user.email))

		return users

	def onload(self):
		for item in self.collection_user:
			user = frappe.get_doc(DOCTYPE.USER, item.email)
			item.full_name = user.full_name

