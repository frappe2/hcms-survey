# Copyright (c) 2022, Mas and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class Survey(Document):
	children = ["collection_survey_element"]
	def before_save(self):
		for elementInstance in self.collection_survey_element:
			# print(elementInstance.in_charge)
			element = frappe.get_doc("Survey Element", elementInstance.survey_element)
			element.in_charge = elementInstance.in_charge
			element.save()



