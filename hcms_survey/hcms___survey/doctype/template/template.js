// Copyright (c) 2022, Mas and contributors
// For license information, please see license.txt

frappe.ui.form.on('Template', {
	onload(frm) {
		if (frm.doc.type !== "") {
			prepareSelectOptionField(frm)
		}
	},

	type(frm) {
		frm.set_value('option', '')
		prepareSelectOptionField(frm)
	}
});

function prepareSelectOptionField(frm) {
	let doctype
	switch (frm.doc.type) {
		case 'Email Event':
			doctype = 'Events'
			frappe.model.with_doctype(doctype, () => {
				let type = frappe.get_meta(doctype).fields.find(df => df.fieldname == 'type')
				frm.set_df_property('option', 'options', type.options)
			})
			break;
		case 'Contract':
			doctype = 'Contract'
			frappe.model.with_doctype(doctype, () => {
				let type, term
				let array = frappe.get_meta(doctype).fields
				for (let index = 0; index < array.length; index++) {
					const element = array[index]
					switch (element.fieldname) {
						case 'type':
							type = element
							break
						case 'term':
							term = element
							break
					}
					if (type && term)
						break
				}
				const contractVal = 'Contract'
				let arrTypeOption = type.options.split('\n')
				let indexContractVal = arrTypeOption.findIndex(val => val == contractVal)
				arrTypeOption.splice(indexContractVal, 1)
				term.options.split('\n').filter(val => val != '').forEach(element => {
					arrTypeOption.splice(indexContractVal, 0, `${contractVal}:${element}`)
				});
				frm.set_df_property('option', 'options', arrTypeOption.join('\n'))
			})
			break;
	}
}
