import json
import frappe
from frappe import _
from frappe.email.doctype.email_template.email_template import EmailTemplate
from frappe.exceptions import DoesNotExistError


@frappe.whitelist(methods="GET")
def get_open_onboard_events_for_employee(employee_name):
    event = frappe.db.sql(
        """
    SELECT tabEvents.* FROM `tabCollection Employee`, tabEvents WHERE `tabCollection Employee`.parent = tabEvents.name AND tabEvents.type='Onboard' AND tabEvents.status='Open' AND `tabCollection Employee`.employee=%(name)s;
    """, values={'name': employee_name}, as_dict=True
    )
    frappe.response.data = event


@frappe.whitelist(methods="POST")
def send_invite_event(event_name):
    event = frappe.get_doc(
        "Events", event_name
    )
    match event.type:
        case 'Onboard':
            email_template = frappe.get_last_doc(
                "Template", filters={
                    'type': 'Email Event',
                    'option': event.type
                })
            list_employee = frappe.get_list(doctype='Employee', fields=['name', 'full_name', 'private_email', 'work_email'], filters={
                'name': ['IN', list(map(lambda emp: emp.employee, event.collection_participant))]
            })
            contact_person = frappe.get_doc(
                "Employee", event.contact_person)
            template_html = frappe.read_file(
                frappe.get_site_path() + email_template.file)
            for emp in list_employee:
                frappe.sendmail(
                    recipients=list(
                        map(lambda emp: emp.private_email, list_employee)),
                    subject=build_email_onboard_subject(email_template, event),
                    message=build_email_onboard_content(
                        template_html, event, emp, contact_person),
                    reference_doctype="Employee",
                    reference_name=emp.name
                )
        case _:
            print('Not Implemented')


def build_email_onboard_subject(template, event):
    return frappe.render_template(template.subject, {
        'start_at': event.start_at})


def build_email_onboard_content(template, event, employee, contact_person):
    return frappe.render_template(template, {'start_at': event.start_at, 'candidate_name': employee.full_name, 'agenda': event.agenda,
                                             'venue': event.venue, 'contact_person': f"{contact_person.full_name} {contact_person.work_phone}"
                                             })


@frappe.whitelist(methods="GET")
def get_content_email_onboard(employee_name, event_name):
    employee = frappe.get_doc("Employee", employee_name)
    event = frappe.get_doc(
        "Events", event_name
    )
    email_template = frappe.get_last_doc(
        "Template", filters={
            'type': 'Email Event',
                    'option': event.type
        })
    contact_person = frappe.get_doc(
        "Employee", event.contact_person)
    template_html = frappe.read_file(
        frappe.get_site_path() + email_template.file)
    frappe.response.data = {
        'subject': build_email_onboard_subject(email_template, event),
        'content': build_email_onboard_content(template_html, event, employee, contact_person)
    }
