from . import __version__ as app_version

app_name = "hcms_survey"
app_title = "HCMS - Survey"
app_publisher = "Mas"
app_description = "Masterise Survey for Frappe"
app_email = "MAS"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = "/assets/hcms_survey/css/pages/hcms_survey.css"
app_include_js = "app.bundle.js"

# include js, css files in header of web template
# web_include_css = "/assets/hcms_survey/css/hcms_survey.css"
# web_include_js = "/assets/hcms_survey/js/hcms_survey.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "hcms_survey/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
page_js = {"survey-detail" : ["public/js/constants.js", "public/js/component_renderer.js"]}

# include js in doctype views
doctype_js = {"events" : "public/js/constants.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Jinja
# ----------

# add methods and filters to jinja environment
# jinja = {
#	"methods": "hcms_survey.utils.jinja_methods",
#	"filters": "hcms_survey.utils.jinja_filters"
# }

# Installation
# ------------

# before_install = "hcms_survey.install.before_install"
# after_install = "hcms_survey.install.after_install"

# Uninstallation
# ------------

# before_uninstall = "hcms_survey.uninstall.before_uninstall"
# after_uninstall = "hcms_survey.uninstall.after_uninstall"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "hcms_survey.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
#	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
#	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

# override_doctype_class = {
#	"ToDo": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
#	"*": {
#		"on_update": "method",
#		"on_cancel": "method",
#		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
#	"all": [
#		"hcms_survey.tasks.all"
#	],
#	"daily": [
#		"hcms_survey.tasks.daily"
#	],
#	"hourly": [
#		"hcms_survey.tasks.hourly"
#	],
#	"weekly": [
#		"hcms_survey.tasks.weekly"
#	],
#	"monthly": [
#		"hcms_survey.tasks.monthly"
#	],
# }

# Testing
# -------

# before_tests = "hcms_survey.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
#	"frappe.desk.doctype.event.event.get_events": "hcms_survey.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
#	"Task": "hcms_survey.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]


# User Data Protection
# --------------------

# user_data_fields = [
#	{
#		"doctype": "{doctype_1}",
#		"filter_by": "{filter_by}",
#		"redact_fields": ["{field_1}", "{field_2}"],
#		"partial": 1,
#	},
#	{
#		"doctype": "{doctype_2}",
#		"filter_by": "{filter_by}",
#		"partial": 1,
#	},
#	{
#		"doctype": "{doctype_3}",
#		"strict": False,
#	},
#	{
#		"doctype": "{doctype_4}"
#	}
# ]

# Authentication and authorization
# --------------------------------

# auth_hooks = [
#	"hcms_survey.auth.validate"
# ]
