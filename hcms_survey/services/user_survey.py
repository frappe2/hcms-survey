import frappe
import json
from hcms_survey.config.constant import *

class UserSurveyService():
	def get_employee(user_email):
		return frappe.get_last_doc(DOCTYPE.EMPLOYEE, {"work_email": user_email})

@frappe.whitelist()
def get(id):
	if not id:
		return False

	userSurvey = frappe.get_doc('User Survey', id)

	dictUserSurvey = userSurvey.as_dict()

	inCharge = {
		"Employee": frappe.get_doc('User', dictUserSurvey["user"]).as_dict(),
		"Line manager": frappe.get_doc('User', dictUserSurvey["line_manager"]).as_dict(),
		"HOD": frappe.get_doc('User', dictUserSurvey["hod"]).as_dict(),
	}

	survey = frappe.get_doc('Survey', dictUserSurvey["survey"]).as_dict()
	survey.user = inCharge["Employee"].name

	survey.elements = []

	for collectionElement in survey.collection_survey_element:
		element = frappe.get_doc('Survey Element', collectionElement.survey_element).as_dict()
		element.name = collectionElement.name
		element.in_charge = collectionElement.in_charge
		element.user_in_charge = inCharge[element.in_charge].name
		element.pivot = collectionElement
		if element.question:
			element.question = get_question(element.question)
			element.question.element_id = element.name
		survey.elements.append(element)

	survey.collection_survey_element = None

	frappe.response.result = RESPONSE_RESULT_SUCCESS
	frappe.response.data = {
		"user": userSurvey.get_user().full_name,
		"survey": survey,
		"answers": json.loads(dictUserSurvey.answers or "{}")
	}

def get_question(id):
	if not id:
		return []

	question = frappe.get_doc('Question', id).as_dict()

	if question.collection_metadata_answer:
		question.metadata_answers = []
		for metadata_answer in question.collection_metadata_answer:
			answer = frappe.get_doc('Metadata Answer', metadata_answer.metadata_answer).as_dict();
			if (answer.collection_question):
				answer.questions = [];
				for metadata_question in answer.collection_question:
					answer.questions.append(get_question(metadata_question.question))
			question.metadata_answers.append(answer)

	if (question.collection_question):
		question.questions = []
		for metadata_question in question.collection_question:
			question.questions.append(get_question(metadata_question.question))

	return question


@frappe.whitelist()

def submit(id, data):
	answers = json.loads(data);
	userSurvey = frappe.get_doc('User Survey', id);

	userSurvey.answers = answers
	userSurvey.save()

	frappe.response.result = RESPONSE_RESULT_SUCCESS
	frappe.response.data = userSurvey
