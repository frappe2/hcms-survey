import frappe
from hcms_survey.config.constant import DOCTYPE

class UserService():
	def get_employee(user_email):
		return frappe.get_last_doc(DOCTYPE.EMPLOYEE, {"work_email": user_email})
