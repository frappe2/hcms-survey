function html_h3(element) {
	return `
		<h3 class="h3-title">${element.index || ""} ${element.label}</h3>
		<span class="h3-description">${element.description}</span>
	`
}

function html_h2(element) {
	return `
		<h2 class="h2-title">${element.index || ""} ${element.label}</h2>
		<span class="h2-description">${element.description}</span>
	`
}

function html_render_element(element, html) {
	if (element.pivot.formula) {
		console.log("🚀 ~ file: component_renderer.js ~ line 17 ~ html_render_element ~ element.pivot.formula", element.pivot.formula)
	}
	return `
	<div style='color: ${element.color}' class="col-md-${COLUMN_BY_PERCENT[element.width]} element-item" data-type="${element.type}">
		${html}
	</div>
	`
}

function html_render_question(question, htmlLabel, editable = "") {
	if (question.type == "Group") {
		return `<div>
					${htmlLabel}
				</div>`
	}

	let html = "";

	switch (question.answer_type) {
		case QUESTION_TYPE_TEXT: {
			html = `
			<div>
				${htmlLabel}
				<input type="text" value="${question.answered}" name="${question.element_id}" class="form-control ${editable}" id="" placeholder="${question.placeholder || ''}">
			</div>
			`
			break;
		}
		case QUESTION_TYPE_NUMBER: {
			html = `
			<div>
				${htmlLabel}
				<input type="number" value="${question.answered}" name="${question.element_id}" class="form-control ${editable}" id="" placeholder="${question.placeholder || ''}">
			</div>
			`
			break;
		}
		case QUESTION_TYPE_TEXTAREA: {
			html = `
			<div>
				${htmlLabel}
				<textarea name="${question.element_id}" class="form-control ${editable}" rows=4 placeholder="${question.placeholder || ''}">${question.answered}</textarea>
			</div>
			`
			break;
		}
		case QUESTION_TYPE_RADIO: {
			for (const item of question.metadata_answers) {
				let htmlExtraQuestion = "";
				if (item.questions) {
					for (const extraQuestion of item.questions) {
						htmlExtraQuestion += `
							<div class="col-md-6"></div>
						`
					}
				}
				html += `
				<div class="radio-item-wrapper">
					<input type="radio" ${editable ? "" : "disabled"} name="${question.element_id}__${question.name}" value="${question.name}" id="a_${question.name}_${item.name}"> <label for="a_${question.name}_${item.name}">${item.label}</label>
					${htmlExtraQuestion}
				</div>
				`
			}
			html = `
			<div class="radio-wrapper flex-column">
				${htmlLabel}
				${html}
			</div>
			`
			break;
		}
	}
	return html;
}

function html_label_question(element) {
	return `<label for="" class="form-label">
		<p class="question-title">${element.index || ""} ${element.question.label}</p>
		<p class="question-description">${element.question.description}</p>
	</label>`
}

function html_matrix(header, body) {
	return `
	<table class="w-100">
		<thead>
			<tr>
			${header}
			</tr>
		</thead>
		<tbody>
			${body}
		</tbody>
	</table>
	`
}
