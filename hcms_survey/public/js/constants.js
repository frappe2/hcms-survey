const ELEMENT_TYPE_H1 = "Heading 1";
const ELEMENT_TYPE_H2 = "Heading 2";
const ELEMENT_TYPE_H3 = "Heading 3";
const ELEMENT_TYPE_QUESTION = "Question";

const QUESTION_TYPE_TEXT = "Text";
const QUESTION_TYPE_TEXTAREA = "Textarea";
const QUESTION_TYPE_RADIO = "Radio";
const QUESTION_TYPE_NUMBER = "Number";

const EVENT_STATE = {
	PENDING: "Pending",
	APPROVED: "Approved",
	REJECTED: "Rejected"
}

const API_URL = {
	GET_USER_SURVEY: "hcms_survey.services.user_survey.get",
	SUBMIT_SURVEY : "hcms_survey.services.user_survey.submit",
	GET_EVENT_PARTICIPANT: "hcms_survey.hcms___survey.doctype.events.events.get_participant",
	EVENT_CHANGE_STATE: "hcms_survey.hcms___survey.doctype.events.events.change_state",
}

const COLUMN_BY_PERCENT = {
	"25%": 3,
	"50%": 6,
	"75%": 9,
	"100%": 12,
}
