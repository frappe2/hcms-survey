import "../templates/pages/detail.html"

frappe.provide('CONSTANTS')

CONSTANTS = {
    ELEMENT_TYPE_H1: "Heading 1",
    ELEMENT_TYPE_H2: "Heading 2",
    ELEMENT_TYPE_H3: "Heading 3",
    ELEMENT_TYPE_QUESTION: "Question",
    
    QUESTION_TYPE_TEXT: "Text",
    QUESTION_TYPE_TEXTAREA: "Textarea",
    QUESTION_TYPE_RADIO: "Radio",
    QUESTION_TYPE_NUMBER: "Number",
    
    EVENT_STATE: {
        PENDING: "Pending",
        APPROVED: "Approved",
        REJECTED: "Rejected"
    },
    
    API_URL: {
        GET_USER_SURVEY: "hcms_survey.hcms___survey.doctype.user_survey.user_survey.get",
        SUBMIT_SURVEY : "hcms_survey.hcms___survey.doctype.user_survey.user_survey.submit",
        GET_EVENT_PARTICIPANT: "hcms_survey.hcms___survey.doctype.events.events.get_participant",
        EVENT_CHANGE_STATE: "hcms_survey.hcms___survey.doctype.events.events.change_state",
    },
    
    COLUMN_BY_PERCENT: {
        "25%": 3,
        "50%": 6,
        "75%": 9,
        "100%": 12,
    }
    

}